import React, {Component} from 'react';

export default class Welcome extends Component {
    constructor(props) {
        super(props);
        this.state = {date: "25"};

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        console.log("clicked");
        var _self = this;

        fetch("http://time.jsontest.com/")
            .then(
                response => response.json()
            ).then(function (jsonData) {
            //handle json data processing here
            _self.setState({date: jsonData.date});
        });
    }

    render() {
        return (
            <div className="xy">
                welcome {this.props.name} {this.state.date}
                <button onClick={this.handleClick} type="button">click</button>
            </div>

        );
    }
}